import EventCapture.EventCapture;
import Initializers.InputValues;
import Initializers.SeleniumInitializer;
import Initializers.WebDriverInitializer;
import ReadJSON.ReadJSON;
import TestPageClasses.HomePage;
import TestPageClasses.RegisterPage;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;


import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class RegisterTest {
    public static EventFiringWebDriver eventFiringWebDriver;
    @Before
    public void setup()throws MalformedURLException,ParseException,IOException{
        WebDriverInitializer webDriverInitializer=new WebDriverInitializer();
        String browserName = System.getProperty("browser");
        //Initialize Browser Driver
        eventFiringWebDriver=webDriverInitializer.getDriver(browserName);
        //Set Implicit Timeout
        eventFiringWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void registerSuccess() throws IOException,ParseException  {
        SeleniumInitializer seleniumInitializer=new SeleniumInitializer();
        ReadJSON readJSON=new ReadJSON();
        InputValues inputValues=new InputValues();
        //Read JSON file data
        inputValues=readJSON.readJSON();
        // Register/Login Process
        seleniumInitializer.registerProcess(eventFiringWebDriver,inputValues);
        String result=eventFiringWebDriver.getTitle();
        assertEquals(result,"Register: Mercury Tours");
    }

    @After
    public  void teardown(){
        eventFiringWebDriver.close();
    }
}
