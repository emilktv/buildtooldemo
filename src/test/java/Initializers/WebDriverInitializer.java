package Initializers;

import EventCapture.EventCapture;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverInitializer {
    public static RemoteWebDriver driver ;
    public static EventFiringWebDriver eventFiringWebDriver;
    public static EventCapture eventCapture;

    public void initializeSelenium(String browser) throws MalformedURLException {
        if(browser.toLowerCase().equals("chrome")){
            ChromeOptions options=new ChromeOptions();
            options.setHeadless(true);
            driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
        }
        else if(browser.toLowerCase().equals("firefox")){
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(true);
            driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
        }
    }
    public void eventFiringWebDriverSetup(){
        this.eventFiringWebDriver=new EventFiringWebDriver(driver);
        this.eventCapture=new EventCapture();
        eventFiringWebDriver.register(eventCapture);
        eventFiringWebDriver.manage().window().maximize();
        String url=System.getProperty("url");
        eventFiringWebDriver.get(url);
    }
    public EventFiringWebDriver getDriver(String browser) throws MalformedURLException{
        initializeSelenium(browser);
        eventFiringWebDriverSetup();
        return eventFiringWebDriver;
    }

}
