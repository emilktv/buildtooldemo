import Initializers.InputValues;
import Initializers.SeleniumInitializer;
import Initializers.WebDriverInitializer;
import ReadJSON.ReadJSON;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class BookFlightTest {
    EventFiringWebDriver eventFiringWebDriver;
    @Before
    public void setup()throws MalformedURLException, ParseException, IOException {
        WebDriverInitializer webDriverInitializer=new WebDriverInitializer();
        String browserName = System.getProperty("browser");
        //Initialize Browser Driver
        eventFiringWebDriver=webDriverInitializer.getDriver(browserName);
        //Set Implicit Timeout
        eventFiringWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void BookFLight() throws IOException,ParseException{
        SeleniumInitializer seleniumInitializer=new SeleniumInitializer();
        InputValues inputValues=new InputValues();
        ReadJSON readJSON=new ReadJSON();
        inputValues=readJSON.readJSON();
        seleniumInitializer.registerProcess(eventFiringWebDriver,inputValues);
        seleniumInitializer.bookFlightProcess(eventFiringWebDriver,inputValues);
    }
    @After
    public  void teardown(){
        eventFiringWebDriver.close();
    }
}
