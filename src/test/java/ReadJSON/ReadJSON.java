package ReadJSON;

import Initializers.InputValues;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class ReadJSON {
    public InputValues readJSON() throws IOException, ParseException {
        Object obj = new JSONParser().parse(new FileReader("./JSON/FlightTest.json"));
        JSONObject jsonObject=(JSONObject) obj;
        return new InputValues((String) jsonObject.get("browser"),(String) jsonObject.get("email"),(String) jsonObject.get("password"),(String) jsonObject.get("confirmPassword"),(String) jsonObject.get("passengerCount"),(String) jsonObject.get("dateDay"),(String) jsonObject.get("toPort"),(String) jsonObject.get("airlineIndexNumber"));
    }
}
