/* Maven Commands executed:
1. mvn clean compile
2. mvn clean test
3. mvn clean test-compile
4. mvn clean failsafe:integration-test failsafe:verify
5. mvn clean package
6. mvn install -DskipTests
7. mvn -Dtest=RegisterTest test

* */

package Initializers;

import EventCapture.EventCapture;
import TestPageClasses.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SeleniumInitializer {
    public static WebDriver driver;
    public static EventFiringWebDriver eventFiringWebDriver;
    public static EventCapture eventCapture;
    //Event Firing WebDriver setup method
    public void eventFiringWebDriverSetup(){
        this.eventFiringWebDriver=new EventFiringWebDriver(driver);
        this.eventCapture=new EventCapture();
        eventFiringWebDriver.register(eventCapture);
        eventFiringWebDriver.manage().window().maximize();
        eventFiringWebDriver.get("http://demo.guru99.com/test/newtours/index.php");
    }
    //Read JSON method. Input Values returned as a class
    public InputValues readJSON() throws IOException,ParseException{
        Object obj = new JSONParser().parse(new FileReader("./JSON/FlightTest.json"));
        JSONObject jsonObject=(JSONObject) obj;
        return new InputValues((String) jsonObject.get("browser"),(String) jsonObject.get("email"),(String) jsonObject.get("password"),(String) jsonObject.get("confirmPassword"),(String) jsonObject.get("passengerCount"),(String) jsonObject.get("dateDay"),(String) jsonObject.get("toPort"),(String) jsonObject.get("airlineIndexNumber"));
    }
    //Initialize WebDriver method
    public void initializeBrowser(String browser) throws MalformedURLException {
        if(browser.toLowerCase().equals("chrome")){
            ChromeOptions options=new ChromeOptions();
            options.setHeadless(true);
            driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
        }
        else if(browser.toLowerCase().equals("firefox")){
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(true);
            driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
        }
    }
    //Register Process
    public void registerProcess( EventFiringWebDriver eventFiringWebDriver,InputValues inputValues){
        //Create Page Objects
        HomePage homePage=new HomePage(eventFiringWebDriver);
        RegisterPage registerPage=new RegisterPage(eventFiringWebDriver);
        RegisterSuccessPage registerSuccessPage=new RegisterSuccessPage(eventFiringWebDriver);
        //Register/Login Process
        homePage.clickRegisterLink();
        registerPage.inputEmail(inputValues.email);
        registerPage.inputPassword(inputValues.password);
        registerPage.inputConfirmPassword(inputValues.confirmPassword);
        registerPage.clickSubmitButton();
        registerSuccessPage.clickHomeLink();
    }
    //Login
    public void loginProcess(EventFiringWebDriver eventFiringWebDriver,InputValues inputValues){
        LoginPage loginPage=new LoginPage(eventFiringWebDriver);
        LoginSuccessPage loginSuccessPage=new LoginSuccessPage(eventFiringWebDriver);
        HomePage homePage=new HomePage(eventFiringWebDriver);
        homePage.clickSignInLink();
        loginPage.inputEmail(inputValues.email);
        loginPage.inputPassword(inputValues.password);
        loginPage.clickSubmitButton();
        loginSuccessPage.clickHomeLink();
    }
    //Book Flight Process
    public void bookFlightProcess(EventFiringWebDriver eventFiringWebDriver,InputValues inputValues){
        HomePage homePage=new HomePage(eventFiringWebDriver);
        LoginSuccessPage loginSuccessPage=new LoginSuccessPage(eventFiringWebDriver);
        FlightFinderPage flightFinderPage=new FlightFinderPage(eventFiringWebDriver);
        homePage.inputEmail(inputValues.email);
        homePage.inputPassword(inputValues.password);
        homePage.clickSubmitButton();
        loginSuccessPage.clickFlightsLink();
        flightFinderPage.selectOneWay();
        flightFinderPage.selectPassengerCount(inputValues.passengerCount);
        flightFinderPage.selectToPort(inputValues.toPort);
        flightFinderPage.selectToDateDay(inputValues.dateDay);
        flightFinderPage.selectBusinessClassRadioButton();
        flightFinderPage.selectAirline(Integer.parseInt(inputValues.airlineIndexNumber));
        flightFinderPage.clickContinueButton();
    }
    public static void main(String[] args) throws IOException,ParseException,MalformedURLException {
        SeleniumInitializer seleniumInitializer=new SeleniumInitializer();
        RegisterSuccessPage registerSuccessPage=new RegisterSuccessPage(eventFiringWebDriver);
        LoginSuccessPage loginSuccessPage=new LoginSuccessPage(eventFiringWebDriver);
        InputValues inputValues=new InputValues();
        //Read JSON file data
        inputValues=seleniumInitializer.readJSON();
        //Initialize Browser Driver
        seleniumInitializer.initializeBrowser(inputValues.browser);
        //Event Firing WebDriver setup
        seleniumInitializer.eventFiringWebDriverSetup();
        //Set Implicit Timeout
        eventFiringWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Register/Login Process
        seleniumInitializer.registerProcess(eventFiringWebDriver,inputValues);

        //Login
        seleniumInitializer.loginProcess(eventFiringWebDriver,inputValues);

        // Find Flight process
        seleniumInitializer.bookFlightProcess(eventFiringWebDriver,inputValues);
        //Close WebDriver
        eventFiringWebDriver.close();
    }
}
